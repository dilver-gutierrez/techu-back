module.exports.config = config;

require('dotenv').config();
//var MLAB_DATABASE_APIKEY = 'apiKey=' + process.env.ENV_MLAB_DATABASE_APIKEY;
//var MLAB_DATABASE_ENDPOINT = 'https://api.mlab.com/api/1/databases/techu26db/collections/';
//var MLAB_DATABASE_ENDPOINT = 'https://api.mlab.com/api/1/databases/techu26db/collections/'; //url alumno
//var MLAB_DATABASE_APIKEY = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
//const MLAB_DATABASE_APIKEY = 'apiKey=' + process.env.ENV_MLAB_DATABASE_APIKEY;

function config(){
  let MLAB_DATABASE_APIKEY    = 'apiKey=' + process.env.ENV_MLAB_DATABASE_APIKEY;
  let MLAB_DATABASE_ENDPOINT  = process.env.ENV_MLAB_DATABASE_ENDPOINT; //'https://api.mlab.com/api/1/databases/techu26db/collections/'; //url alumno
  let SERVICE_PATH_BASE       = process.env.ENV_SERVICE_PATH_BASE; //'/apitechu/v1/';
  let SERVICE_PATH_BASE_V2    = process.env.ENV_SERVICE_PATH_BASE_V2; //'/apitechu/v2/collections/';
  let PORT                    = process.env.ENV_PORT || 3000;
  return {'MLAB_DATABASE_APIKEY': MLAB_DATABASE_APIKEY,
          'MLAB_DATABASE_ENDPOINT': MLAB_DATABASE_ENDPOINT,
          'SERVICE_PATH_BASE': SERVICE_PATH_BASE,
          'SERVICE_PATH_BASE_V2': SERVICE_PATH_BASE_V2,
          'PORT': PORT
          };
}
