//console.log("Hola node");
var express = require('express');
var userFile = require('./user.json');
var fs = require('fs');
var jsonDataBaseUsers = JSON.stringify(userFile); //'{"data" : ' + JSON.stringify(userFile) + '}';
var bodyParser = require('body-parser');
var requestJson = require('request-json');

const cors = require('cors');

//Importar otros archivos para utilizar sus funciones
const CONFIG = require('./config/conf').config();
const account_controller = require('./controllers/account_controller');

var app = express();
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

var totalUsers = 0;


//POST login v2
app.post(CONFIG.SERVICE_PATH_BASE_V2 + 'login',
		function(req, res) {
      let email = req.body.email;
      let password = req.body.password;
      var queryString = 'q={"email":"' + email + '","password":"' + password + '"}&';
      console.log('Local POST ' + CONFIG.SERVICE_PATH_BASE_V2 + 'login');

      var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
      let putLogin = '{"$set":{"logged":true}}';

      httpClient.put(CONFIG.MLAB_DATABASE_ENDPOINT + 'user?' + queryString + CONFIG.MLAB_DATABASE_APIKEY, JSON.parse(putLogin),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
        let respuesta = body;
        res.status(500);
        if (body.n == 1) {
          res.status(200);
          respuesta = {"msg":"Login correcto"};
        }
        res.send(respuesta);
      });
		}
);

//DELETE user con parámetro 'id'
app.delete(CONFIG.SERVICE_PATH_BASE_V2 + 'users/:id',
		function(req, res) {
      var id = req.params.id;
      var queryStringID = 'q={"userID":' + id + '}&';
      console.log('Local DELETE ' + CONFIG.SERVICE_PATH_BASE_V2 + 'users/' + id);
      console.log('queryStringID = ' + queryStringID);
      var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
      httpClient.get('user?'+ queryStringID + CONFIG.MLAB_DATABASE_APIKEY,
      function(error, respuestaMLab, body) {
        var datoRemoto = body[0];
        httpClient.delete(CONFIG.MLAB_DATABASE_ENDPOINT +'user/' + datoRemoto._id.$oid + '?' + CONFIG.MLAB_DATABASE_APIKEY,
        function(error, respuestaMLab, body) {
          console.log("body:"+ body);
          let respuesta = body;
          /*
          res.status(500);
          if (body.n == 1) {
            res.status(200);
            respuesta = {"msg":"Usuario actualizado correctamente"};
          }
          */
          res.send(respuesta);
        });
      });
		}
);

//PUT users con parámetro 'id'
app.put(CONFIG.SERVICE_PATH_BASE_V2 + 'users/:id',
		function(req, res) {
      var id = req.params.id;
      var queryStringID = 'q={"userID":' + id + '}&';
      console.log('Local PUT ' + CONFIG.SERVICE_PATH_BASE_V2 + 'users/' + id);
      console.log('queryStringID = ' + queryStringID);
      var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
      httpClient.get('user?'+ queryStringID + CONFIG.MLAB_DATABASE_APIKEY,
      function(error, respuestaMLab, body) {
        var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
        console.log(req.body);
        console.log(cambio);
        httpClient.put(CONFIG.MLAB_DATABASE_ENDPOINT +'user?' + queryStringID + CONFIG.MLAB_DATABASE_APIKEY, JSON.parse(cambio),
        function(error, respuestaMLab, body) {
          console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
          let respuesta = body;
          res.status(500);
          if (body.n == 1) {
            res.status(200);
            respuesta = {"msg":"Usuario actualizado correctamente"};
          }
          res.send(respuesta);
        });
      });
		}
);

//Peticion POST de un 'user' ()
app.post(CONFIG.SERVICE_PATH_BASE_V2 + 'users',
  function(req, res){

    console.log('Local POST ' + CONFIG.SERVICE_PATH_BASE_V2 + 'users');
    var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
    var fields = 'f={"_id":0,"password":0}&';
    //var queryString = 'q={"logged":false}&';
    var limit = 'l=1&';
    var sort = 's={"userID": -1}&';
    let urlGetRemoto = CONFIG.MLAB_DATABASE_ENDPOINT + 'user' + '?' + limit + sort + CONFIG.MLAB_DATABASE_APIKEY;
    console.log('urlGetRemoto GET ' + urlGetRemoto);
    httpClient.get(urlGetRemoto,
      function(err, resRemote, body){
        var respLocal = {};
        console.log(body.length);

        let newID = body.length > 0 ? body[0].userID + 1 : 1;
        console.log("newID:" + newID);
        var newUser = {
         "userID" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
        };
        httpClient.post(CONFIG.MLAB_DATABASE_ENDPOINT + "user?" + CONFIG.MLAB_DATABASE_APIKEY, newUser,
        function(error, respuestaMLab, body){
          console.log(body);
          res.status(201);
          res.send(body);
        });

      }
    );

});

//Peticion GET de todos los 'users' (Collections)
app.get(CONFIG.SERVICE_PATH_BASE_V2 + 'users', account_controller.getUsers);

//Peticion GET de todos los 'user' ()
app.get(CONFIG.SERVICE_PATH_BASE_V2 + 'users/:id', account_controller.getUser);

//Peticion GET de todas las cuentas del 'user' ()
app.get(CONFIG.SERVICE_PATH_BASE_V2 + 'users/:id/accounts', account_controller.getUserAccounts);

//Peticion GET de todos los 'users' (Collections)
app.get(CONFIG.SERVICE_PATH_BASE + 'users',
  //app.get('/',
  function(req, res){
    console.log('GET ' + CONFIG.SERVICE_PATH_BASE + 'users');
    res.send(jsonDataBaseUsers);
});

//Peticion GET de un 'user' (Instance)
app.get(CONFIG.SERVICE_PATH_BASE + 'users/:id',
  function(req, res){
    console.log('GET ' + CONFIG.SERVICE_PATH_BASE + 'users/id');
    let indice = req.params.id;
    let instancia = userFile[indice - 1]
    let respuesta = instancia != undefined ? instancia : {"mensaje":"recurso no encontrado"};
    res.status(200);
    res.send(respuesta);
  }
);

//Peticion GET de varios 'user' (Collection) segun query
app.get(CONFIG.SERVICE_PATH_BASE + 'usersq',
  function(req, res){
    console.log('GET ' + CONFIG.SERVICE_PATH_BASE + 'con query string');
    let queryLoggedCondition = (req.query.logged == "false") ? 0 : ((req.query.logged == "true") ? 1 : 2);
    let beginIndex = req.query.beginIndex > 0 ? req.query.beginIndex : 1;
    let size = req.query.size > 0 ? req.query.size : userFile.length - beginIndex + 1;
    let count = 0;
    let i = 0;
    let users = userFile;
    let returnUsers = [];
    for (i = beginIndex - 1; count < size && i < userFile.length; i++) {
      let u = userFile[i];
      let userLoggedCondition = (u.logged == true) ? 1 : 0;
      if (queryLoggedCondition == 2 || queryLoggedCondition == userLoggedCondition) {
        returnUsers.push(u);
        count++;
      }
    }
    res.status(200);
    res.send(returnUsers);
  }
);

//Peticion POST para crear un usuario nuevo
app.post(CONFIG.SERVICE_PATH_BASE + 'users',
  function(req, res){
      totalUsers = userFile.length;
      let isValid = (
            req.body.first_name   != null && req.body.first_name  != "" &&
            req.body.last_name    != null && req.body.last_name   != "" &&
            req.body.email        != null && req.body.email       != "" &&
            req.body.password     != null && req.body.password    != ""
    );
    if(isValid) {
      let newUser = {
        userID : totalUsers,
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        email : req.body.email,
        password : req.body.password
      }
      userFile.push(newUser);
      res.status(200);
      res.send({"mensaje" : "Usuario creado con éxito.", "usuario" : newUser});
    } else {
      res.status(500);
      res.send({"mensaje" : "Ingrese todos los datos de usuario."});
    }

  }
);

//Peticion PUT para modificar los datos de un usuario
app.put(CONFIG.SERVICE_PATH_BASE + 'users/:id',
  function(req, res){
      //totalUsers = userFile.length;
      let indice = req.params.id;
      let isValid = (
        (req.body.first_name   != null && req.body.first_name  != "") ||
        (req.body.last_name    != null && req.body.last_name   != "") ||
        (req.body.email        != null && req.body.email       != "") ||
        (req.body.password     != null && req.body.password    != "")
    ) && (indice < userFile.length);
    if(isValid) {

      console.log('PUT ' + CONFIG.SERVICE_PATH_BASE + 'users');

      let instancia = userFile[indice - 1];
      let respuesta = instancia != undefined ? instancia : {"mensaje":"recurso no encontrado"};
      instancia.first_name = req.body.first_name,
      instancia.last_name = req.body.last_name,
      instancia.email = req.body.email,
      instancia.password = req.body.password
      userFile[indice - 1] = instancia;
      res.status(201);
      res.send({"mensaje" : "Usuario modificado con éxito.", "usuario" : instancia});
    } else {
      res.status(500);
      res.send({"mensaje" : "Error al actualizar usuario."});
    }

  }
);

//Peticion DELETE de un 'user' (Instance)
//Hacerlo en el body cuando sea un dato sensible
app.delete(CONFIG.SERVICE_PATH_BASE + 'users/:id',
  function(req, res){
    console.log('DELETE ' + CONFIG.SERVICE_PATH_BASE + 'users/id');
    let indice = req.params.id;
    let instanciaEliminar = userFile[indice - 1];
    instanciaEliminar = userFile.splice(indice - 1,1);
    let respuesta = instanciaEliminar != undefined ? instanciaEliminar : {"mensaje":"recurso no encontrado"};
    res.status(204);
    res.send(respuesta);
  }
);

//Login de usuario
app.post(CONFIG.SERVICE_PATH_BASE + 'login',
  function(req, res){
    console.log('POST ' + CONFIG.SERVICE_PATH_BASE + 'login');
    let logged = false;
    let isValid = (
        (req.body.email        != null && req.body.email       != "") &&
        (req.body.password     != null && req.body.password    != "")
    );
    var users = userFile;
    if(isValid) {
      for (u of users) {
        if (req.body.email == u.email && req.body.password == u.password) {
          u.logged = true;
          logged = true;
          break;
        }
      }

      if (logged) {
        writeUserDataToFile(users);
        res.status(200);
        res.send({"mensaje" : "Sesión iniciada correctamente."});
      } else {
        res.status(400);
        res.send({"mensaje" : "Ocurrió un error de acceso, el usuario o clave no son correctos."});
      }

    } else {
      res.status(500);
      res.send({"mensaje" : "Error validar usuario."});
    }

  }
);

//Logout de usuario
app.post(CONFIG.SERVICE_PATH_BASE + 'logout',
  function(req, res){
    console.log('POST ' + CONFIG.SERVICE_PATH_BASE + 'logout');
    let logged = false;
    let isValid = (
        (req.body.email        != null && req.body.email       != "")
    );
    var users = userFile;
    if(isValid) {
      for (u of users) {
        if (req.body.email == u.email && u.logged != null && u.logged == true) {
          delete u.logged;
          logged = true;
          break;
        }
      }

      if (logged) {
        writeUserDataToFile(users);
        res.status(200);
        res.send({"mensaje" : "Sesión finalizada correctamente."});
      } else {
        res.status(400);
        res.send({"mensaje" : "Usuario no logeado."});
      }
    } else {
      res.status(500);
      res.send({"mensaje" : "Error al validar usuario."});
    }

  }
);

function writeUserDataToFile(data) {
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(CONFIG.PORT, function() {
  console.log('API escuchando en puerto 3000...');
});
