module.exports.getUsers = getUsers;
module.exports.getUser = getUser;
module.exports.getUserAccounts = getUserAccounts;
var requestJson = require('request-json');

//Importar otros archivos para utilizar sus funciones
const CONFIG = require('../config/conf').config();

//Peticion GET de las cuentas de un 'user'
function getUserAccounts(req, res){
  var id = req.params.id;
  console.log('GET ' + CONFIG.SERVICE_PATH_BASE_V2 + 'users/' + id + '/accounts');
  var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
  var fields = 'f={"_id":0,"password":0}&';
  var queryStringID = 'q={"userId":' + id + '}&';
  let urlGetRemoto = CONFIG.MLAB_DATABASE_ENDPOINT + 'userAccount' + '?' + fields + queryStringID + CONFIG.MLAB_DATABASE_APIKEY;
  console.log('urlGetRemoto GET ' + urlGetRemoto);
  httpClient.get(urlGetRemoto,
    function(err, resRemote, body){
      var respLocal = {};
      console.log(body.length);
      if(err) {
        response = {"msg" : "Error obteniendo las cuentas de usuario."}
        res.status(500);
      } else {
        res.status(200);
        respLocal = body;
        res.send(respLocal);
      }
    }
  );
}

//Peticion GET de todos los 'users' (Collections)
function getUsers(req, res){
  console.log('GET ' + CONFIG.SERVICE_PATH_BASE_V2 + 'users');
  var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
  var fields = 'f={"_id":0,"password":0}&';
  var queryString = '';//'q={"logged":false}&';
  let urlGetRemoto = CONFIG.MLAB_DATABASE_ENDPOINT + 'user' + '?' + fields + queryString + CONFIG.MLAB_DATABASE_APIKEY;
  console.log('urlGetRemoto GET ' + urlGetRemoto);
  httpClient.get(urlGetRemoto,
    function(err, resRemote, body){
      var respLocal = {};
      console.log(body.length);
      if(err) {
        response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
      } else {
        res.status(200);
        respLocal = body;
        res.send(respLocal);
      }
    }
  );
}

//Peticion GET de todos los 'user' ()
function getUser(req, res){
  var id = req.params.id;
  console.log('GET ' + CONFIG.SERVICE_PATH_BASE_V2 + 'users/' + id);
  var httpClient = requestJson.createClient(CONFIG.MLAB_DATABASE_ENDPOINT);
  var fields = 'f={"_id":0,"password":0}&';
  var queryStringID = 'q={"userID":' + id + '}&';
  let urlGetRemoto = CONFIG.MLAB_DATABASE_ENDPOINT + 'user' + '?' + fields + queryStringID + CONFIG.MLAB_DATABASE_APIKEY;
  console.log('urlGetRemoto GET ' + urlGetRemoto);
  httpClient.get(urlGetRemoto,
    function(err, resRemote, body){
      var respLocal = {};
      console.log(body.length);
      if(err) {
        response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
      } else {
        res.status(200);
        respLocal = body;
        res.send(respLocal);
      }
    }
  );
}
