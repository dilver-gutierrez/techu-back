var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();

// Ver: https://www.paradigmadigital.com/dev/testeo-api-rest-mocha-chai-http/?utm_source=Site&utm_campaign=Related_Posts_Right&utm_medium=Blog

describe('test unitario users',
  function() { // Funcion manejadora de la suite
    it('Test de API que devuelve las lista de usuarios correctos', //Test unitario
      function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v2/collections/users') //Establecemos prueba para GET
          .end(
            function(err,res){ //Definiciòn de asercionesS
              console.log('Resquest terminada');
              res.should.have.status(200);
              res.body.should.be.a('array');
              for (user of res.body) {
                user.should.have.property('first_name');
                user.should.have.property('last_name');
              }
              done(); //Terminamos de evaluar las aserciones
            }
          )
      });
  }
);
